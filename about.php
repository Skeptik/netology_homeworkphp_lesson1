<!DOCTYPE html>
<html lang="ru">
<head>
	<title>Lesson1</title>
	<meta charset="utf-8">
<style type="text/css">

body{
	background: #00b85c;
}

h1{
	font-family: Courier;
	background-color: #7FFF00;
}

.table{
	display: table-row;
	font-size: 20px;
	font-weight: bold;
	color: #FFFFFF;
}

div{
	display: table-cell;
	padding: 5px;
	padding-right: 30px;
}
</style>
</head>
<body>
	<?php
	$name = 'Александр';
	$age = 28;
	$mail = 'Zhnecc@gmail.com';
	$city = 'Воронеж';
	$about = 'Студент "Нетологии"';
	?>
	<h1>Страница пользователя <?= $name ?></h1>
	<div class="table">
		<div>Имя:</div>
		<div><?= $name ?></div>
	</div>
	<div class="table">
		<div>Возраст:</div>
		<div><?= $age ?></div>
	</div>
	<div class="table">
		<div>Адрес электронной почты:</div>
		<div><a href="mailto:<?= $mail ?>"><?= $mail ?></a></div>
	</div>
	<div class="table">
		<div>Город:</div>
		<div><?= $city ?></div>
	</div>
	<div class="table">
		<div>О себе:</div>
		<div><?= $about ?></div>
	</div>
</body>
</html>